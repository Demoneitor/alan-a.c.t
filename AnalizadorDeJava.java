
package analizadordejava;

import com.sun.webkit.ContextMenu;
import java.awt.JobAttributes;
import java.util.Scanner;
import java.io.*;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Random;

public class AnalizadorDeJava {

    static int cantidad = 100;
    static String arrInt[] = new String[cantidad];
    static String arrString[] = new String[cantidad];
    static String arrBool[] = new String[cantidad];
    static String arrChar[] = new String[cantidad];
    static String arrByte[] = new String[cantidad];
    static String arrShort[] = new String[cantidad];
    static String arrLong[] = new String[cantidad];
    static String arrFloat[] = new String[cantidad];
    static String arrDouble[] = new String[cantidad];
    static int movInt=0,movString=0,movBool=0,movChar=0,movByte=0,movShort=0,
            movLong=0,movFloat=0,movDouble=0;
    
    public static void main(String[] args) {
        
        String cad;
        int i,j,cont=0;
        Scanner leer= new Scanner(System.in);
        
       
       
       String [] tokens = {"boolean","char","byte","short","int","long","float",
           "double","package","public","class","static","void","for","if",
           "while","do","case","switch","break","catch","new","return",
           "protected","private","const","default","else","extends","final",
           "finally","goto","implements","import","interface","super","this",
           "throw","throws","try","true","null","false","String"};
       
       int [] contador = new int [tokens.length];
       
      
     /*
       do  //Para ver si se repiten tokens :,)
       {
       
        System.out.println("Ingresa cadena");
            cad= leer.nextLine();
      
            
         for (i=0;i<tokens.length;i++)
         {
             if(tokens[i].equals(cad))
             {
                 System.out.println("si");  
             }
         }
            
            
       } while(!cad.equals("0"));    
      */
     
     
     for(i=0;i<arrInt.length;i++)
     {
         arrInt[i]="";
         arrString[i]="";
         arrBool[i]="";
         arrChar[i]="";
         arrByte[i]="";
         arrShort[i]="";
         arrLong[i]="";
         arrFloat[i]="";
         arrDouble[i]="";
     }
     
    try {
      FileReader fr = new FileReader("programaJava2.txt");
      BufferedReader br = new BufferedReader(fr);
      
      i=0;
      String linea;
      
      while ((linea = br.readLine()) != null )  //Almacena el renglon en linea
      {
        //System.out.println(linea);
          /*
          if (linea.contains("int"))
          {
              System.out.println("si");
          }*/
                  
             //Separa la linea en un arreglo de palabras
          String [] palabras = linea.split(" ");
          
          
          String cadena=linea;

           //Para direccion memoria Int
        Pattern pat = Pattern.compile                             //
   ("\\s*int\\s+([a-zA-Z0-9_]+|[a-zA-Z0-9_]+\\s*=\\s*-{0,1}\\s*[0-9]{1,10})\\s*"
           + "((,\\s*[a-zA-Z0-9_]+\\s*)*(,\\s*[a-zA-Z0-9_]+\\s*=\\s*-{0,1}\\s*"
           + "[0-9]{1,10}\\s*)*)*;");
           
      Matcher mat = pat.matcher(cadena);
      
     if (mat.matches()) {
        // System.out.println(cont);
         DeclaInt(palabras);
     }
        //Para direccion memoria String
       Pattern pat2 = Pattern.compile                             //
   ("\\s*String\\s+([a-zA-Z0-9_]+|[a-zA-Z0-9_]+\\s*=\\s*((\"[a-zA-Z0-9\\s_-]"
           + "{0,}\")|null))\\s*((\\s*,\\s*[a-zA-Z0-9_]+\\s*)*(\\s*,\\s*"
           + "[a-zA-Z0-9_]+\\s*=\\s*((\"[a-zA-Z0-9\\s_-]{0,}\")|null)\\s*"
           + ")*)*;");
           
      Matcher mat2 = pat2.matcher(cadena);
     if (mat2.matches()) {
        // System.out.println(cont);
        DeclaString(palabras);
     }
     
     
     //Para direccion memoria boolean
       Pattern pat3 = Pattern.compile     
   ("\\s*boolean\\s+([a-zA-Z0-9_]+|[a-zA-Z0-9_]+\\s*=\\s*(true|false))\\s*"
           + "((,\\s*[a-zA-Z0-9_]+\\s*)*(,\\s*[a-zA-Z0-9_]+\\s*=\\s*"
           + "(true|false)\\s*)*)*\\s*;");
           
      Matcher mat3 = pat3.matcher(cadena);
     if (mat3.matches()) {
        // System.out.println(cont);
        DeclaBool(palabras);
     }
     
     
     //Para direccion memoria char
       Pattern pat4 = Pattern.compile                             //
   ("\\s*char\\s+([a-zA-Z0-9_]+|[a-zA-Z0-9_]+\\s*=\\s*(('[a-zA-Z0-9\\s_-]"
           + "{1}')))\\s*((\\s*,\\s*[a-zA-Z0-9_]+\\s*)*(\\s*,\\s*[a-zA-Z0-9_]+"
           + "\\s*=\\s*(('[a-zA-Z0-9\\s_-]{1}')|null)\\s*)*)*;");
           
      Matcher mat4 = pat4.matcher(cadena);
     if (mat4.matches()) {
        // System.out.println(cont);
        DeclaChar(palabras);
     }
     
     //Para direccion memoria byte
       Pattern pat5 = Pattern.compile                             //
   ("\\s*byte\\s+([a-zA-Z0-9_]+|[a-zA-Z0-9_]+\\s*=\\s*-{0,1}\\s*[0-9]{1,3})"
           + "\\s*((,\\s*[a-zA-Z0-9_]+\\s*)*(,\\s*[a-zA-Z0-9_]+\\s*=\\s*-{0,1}"
           + "\\s*[0-9]{1,3}\\s*)*)*\\s*;");
           
      Matcher mat5 = pat5.matcher(cadena);
     if (mat5.matches()) {
        // System.out.println(cont);
        DeclaByte(palabras);
     }
     
     
     //Para direccion memoria short
       Pattern pat6 = Pattern.compile                             //
   ("\\s*short\\s+([a-zA-Z0-9_]+|[a-zA-Z0-9_]+\\s*=\\s*-{0,1}\\s*[0-9]{1,5})"
           + "\\s*((,\\s*[a-zA-Z0-9_]+\\s*)*(,\\s*[a-zA-Z0-9_]+\\s*=\\s*-{0,1}"
           + "\\s*[0-9]{1,5}\\s*)*)*\\s*;");
           
      Matcher mat6 = pat6.matcher(cadena);
     if (mat6.matches()) {
        // System.out.println(cont);
        DeclaShort(palabras);
     }
     
     
     //Para direccion memoria long
       Pattern pat7 = Pattern.compile                             //
   ("\\s*long\\s+([a-zA-Z0-9_]+|[a-zA-Z0-9_]+\\s*=\\s*-{0,1}\\s*[0-9]{1,19})"
           + "\\s*((,\\s*[a-zA-Z0-9_]+\\s*)*(,\\s*[a-zA-Z0-9_]+\\s*=\\s*-{0,1}"
           + "\\s*[0-9]{1,19}\\s*)*)*\\s*;");
           
      Matcher mat7 = pat7.matcher(cadena);
     if (mat7.matches()) {
        // System.out.println(cont);
        DeclaLong(palabras);
     }
     
     
     
     //Para direccion memoria float
       Pattern pat8 = Pattern.compile                             //
   ("\\s*float\\s+([a-zA-Z0-9_]+|[a-zA-Z0-9_]+\\s*=\\s*-{0,1}\\s*[0-9]{1,}(."
           + "[0-9]{1,}f)*)\\s*((,\\s*[a-zA-Z0-9_]+\\s*)*(,\\s*[a-zA-Z0-9_]+"
           + "\\s*=\\s*-{0,1}\\s*[0-9]{1,}(.[0-9]{1,}f)*\\s*)*)*\\s*;");
           
      Matcher mat8 = pat8.matcher(cadena);
     if (mat8.matches()) {
        // System.out.println(cont);
        DeclaFloat(palabras);
     }
     
     
      //Para direccion memoria Double
       Pattern pat9 = Pattern.compile                             //
   ("\\s*double\\s+([a-zA-Z0-9_]+|[a-zA-Z0-9_]+\\s*=\\s*-{0,1}\\s*[0-9]{1,}"
           + "(.[0-9]{1,}f{0,1})*)\\s*((,\\s*[a-zA-Z0-9_]+\\s*)*(,\\s*"
           + "[a-zA-Z0-9_]+\\s*=\\s*-{0,1}\\s*[0-9]{1,}(.[0-9]{1,}f{0,1})"
           + "*\\s*)*)*\\s*;");
           
      Matcher mat9 = pat9.matcher(cadena);
     if (mat9.matches()) {
        // System.out.println(cont);
        DeclaDouble(palabras);
     }
     
          
     
          
          //Metodo para eliminar tabuladores
          
         try
          {
              
          if (palabras[0].charAt(0)=='\t')
          {
              palabras[0]=tabulador(palabras[0], tokens);
              System.out.println(palabras[0]);
          }
          
          }
          catch (ArrayIndexOutOfBoundsException e)
          {}
          catch (StringIndexOutOfBoundsException e)
          {}
          /////////////
          
          
          int a,r,t=2;
       
          
          for (i=0;i<palabras.length;i++)
          {
              for (j=0;j<tokens.length;j++)
              {
                if (palabras[i].equals(tokens[j]))
                {
                      contador[j]+=1;
                }
              }
          }        
      }
      fr.close();
      System.out.println("Token \t Contador\n");
      
      for (i=0;i<tokens.length;i++)
      {
          if (contador[i]!=0)
          {
          System.out.print(tokens[i]+"\t\t");
          System.out.println(contador[i]);
          }
      }
        System.out.println("\n\n\tDirecciones de memoria");
        System.out.println("\n\n___ Variable ___\t___ Direccion ___");
      
        Random r = new Random();
        
        //Imprime direcciones memoria Int//
        System.out.println("\n----- Enteros -----\n");
      for (i=0;i<arrInt.length;i++)
      {
            
          if (!arrInt[i].equals(""))
          {
              int numero = r.nextInt(10000)+10000;
              
              System.out.print(arrInt[i]);
              System.out.print("\t\t\t\t"+numero);
              System.out.println("");
          }
      }
       
      //Imprime direcciones memoria String//
        System.out.println("\n----- String -----\n");
       for (i=0;i<arrString.length;i++)
      {
            
          if (!arrString[i].equals(""))
          {
              int numero = r.nextInt(10000)+10000;
              
              System.out.print(arrString[i]);
              System.out.print("\t\t\t\t"+numero);
              System.out.println("");
          }
      }
       
      //Imprime direcciones memoria boolean//
        System.out.println("\n----- boolean -----\n");
       for (i=0;i<arrBool.length;i++)
      {
            
          if (!arrBool[i].equals(""))
          {
              int numero = r.nextInt(10000)+10000;
              
              System.out.print(arrBool[i]);
              System.out.print("\t\t\t\t"+numero);
              System.out.println("");
          }
      }
       
       //Imprime direcciones memoria char//
        System.out.println("\n----- char -----\n");
       for (i=0;i<arrChar.length;i++)
      {
            
          if (!arrChar[i].equals(""))
          {
              int numero = r.nextInt(10000)+10000;
              
              System.out.print(arrChar[i]);
              System.out.print("\t\t\t\t"+numero);
              System.out.println("");
          }
      }
       
       
       
        //Imprime direcciones memoria byte//
        System.out.println("\n----- byte -----\n");
       for (i=0;i<arrByte.length;i++)
      {
            
          if (!arrByte[i].equals(""))
          {
              int numero = r.nextInt(10000)+10000;
              
              System.out.print(arrByte[i]);
              System.out.print("\t\t\t\t"+numero);
              System.out.println("");
          }
      }
       
       
       
        //Imprime direcciones memoria short//
        System.out.println("\n----- short -----\n");
       for (i=0;i<arrShort.length;i++)
      {
            
          if (!arrShort[i].equals(""))
          {
              int numero = r.nextInt(10000)+10000;
              
              System.out.print(arrShort[i]);
              System.out.print("\t\t\t\t"+numero);
              System.out.println("");
          }
      }
       
       
       
       //Imprime direcciones memoria long//
        System.out.println("\n----- long -----\n");
       for (i=0;i<arrLong.length;i++)
      {
            
          if (!arrLong[i].equals(""))
          {
              int numero = r.nextInt(10000)+10000;
              
              System.out.print(arrLong[i]);
              System.out.print("\t\t\t\t"+numero);
              System.out.println("");
          }
      }
       
       
       
       //Imprime direcciones memoria float//
        System.out.println("\n----- float -----\n");
       for (i=0;i<arrFloat.length;i++)
      {
            
          if (!arrFloat[i].equals(""))
          {
              int numero = r.nextInt(10000)+10000;
              
              System.out.print(arrFloat[i]);
              System.out.print("\t\t\t\t"+numero);
              System.out.println("");
          }
      }
       
       
       
       //Imprime direcciones memoria Double//
        System.out.println("\n----- Double -----\n");
       for (i=0;i<arrDouble.length;i++)
      {
            
          if (!arrDouble[i].equals(""))
          {
              int numero = r.nextInt(10000)+10000;
              
              System.out.print(arrDouble[i]);
              System.out.print("\t\t\t\t"+numero);
              System.out.println("");
          }
      }
       
       
       
    }
    catch (Exception e) {
      System.out.println("Excepcion leyendo fichero" + e);
       
    }
    
    
    
    }
    
    public static String tabulador(String palabras,String[] tokens)
    {
        int i,j;
        
        for (i=0;i<tokens.length;i++)
        {
            if (palabras.contains(tokens[i]))
                return tokens[i];
        }
        
        
        return "N";
    }
    
      //Metodo para encontrar Declaraciones de INT//
    public static void DeclaInt(String[] cad)
    {
      int i,j=0;
      for (i=0;i<cad.length;i++)
      {
          
          
          Pattern pat = Pattern.compile("[a-zA-Z-0-9,;=_-]+");
          
          Matcher mat = pat.matcher(cad[i]);
          if (mat.matches()) 
          {
            if (!cad[i].equals("int"))
            {
                //System.out.println(cad[i]);
               Pattern pat2 = Pattern.compile("[a-zA-Z]+[0-9]*"); 
               Matcher mat2 = pat2.matcher(cad[i]);
               if (mat2.matches())
               {
                  // System.out.println("si");
                   arrInt[movInt]=cad[i];
                   movInt++;
               } else
               {
                
                
                   // System.out.println("si");
                   // System.out.println(cad[i]);
                    boolean igual = false;
                    boolean coma = false;
                    for (j=0;j<cad[i].length()-1;j++)
                    {
                        
                            switch (cad[i].charAt(j))
                            {
                                case '=':
                                {
                                    igual= true ;
                                    break;
                                }
                                
                                case ',':
                                {
                                    coma= true ;
                                    igual= false ;
                                    movInt++;
                                    break ;
                                }
                                
                                case ';':
                                {
                                    j=cad[i].length();
                                    break ;
                                }
                                
                                
                                
                                default :
                                {
                                  Pattern pat3 = Pattern.compile
                                    ("[a-zA-Z]+[0-9]*"); 
                                  Matcher mat3 = pat3.matcher
                                    (Character.toString(cad[i].charAt(j)));
                                  if (mat3.matches())  
                                  {
                                    if (igual==false)
                                    {
                                        arrInt[movInt]+=cad[i].charAt(j);
                                    }
                                  } 
                                }
                                        
                            
                            
                            }
                            
                    }
                   movInt++;
                   // System.out.println("");
                
               }
               
            }
          }
          
      }  
    }
    
    
    //Metodo para encontrar Declaraciones de String//
    public static void DeclaString(String[] cad)
    {
      int i,j=0;
      boolean empiezaCad=false;
      for (i=0;i<cad.length;i++)
      {
           
          
          Pattern pat = Pattern.compile("[a-zA-Z-0-9,;\"=_-]+");
          
          Matcher mat = pat.matcher(cad[i]);
          if (mat.matches()) 
          {
            if (!cad[i].equals("String"))
            {
                //System.out.println(cad[i]);
               Pattern pat2 = Pattern.compile("[a-zA-Z]+[0-9]*"); 
               Matcher mat2 = pat2.matcher(cad[i]);
               if (mat2.matches() && empiezaCad==false)
               {
                  // System.out.println("si");
                   arrString[movString]=cad[i];
                   movString++;
               } else
               {
                   
                   // System.out.println("si");
                   // System.out.println(cad[i]);
                    boolean igual = false;
                    boolean coma = false;
                  
                    
                    for (j=0;j<cad[i].length()-1;j++)
                    {
                        
                
                            switch (cad[i].charAt(j))
                            {
                                case '=':
                                {
                                    igual= true ;
                                    break;
                                }
                                
                                case ',':
                                {
                                    coma= true ;
                                    igual= false ;
                                    movString++;
                                    break ;
                                }
                                
                                case ';':
                                {
                                    j=cad[i].length();
                                    break ;
                                }
                                
                                case '"':
                                {
                                    if(empiezaCad==false)
                                    {
                                        empiezaCad=true;
                                    }else
                                    {
                                        empiezaCad=false;
                                    }
                                    
                                  break;
                                }
                                
                                default :
                                {
                                  Pattern pat3 = Pattern.compile
                                    ("[a-zA-Z]+[0-9]*"); 
                                  Matcher mat3 = pat3.matcher
                                    (Character.toString(cad[i].charAt(j)));
                                  if (mat3.matches() )  
                                  {
                                    if (igual==false && empiezaCad==false)
                                    {
                                        arrString[movString]+=cad[i].charAt(j);
                                    }
                                  } 
                                }
                                
                               if(arrString[movString].equals("null"))
                               {
                                   arrString[movString]="";
                                   movString--;
                               }
                                        
                            
                            
                            }
                            
                    }
                   movString++;
                   // System.out.println("");
                
               }
               
            }
          }
          
      }  
    }
    
    
    //Metodo para encontrar Declaraciones de boolean//
    public static void DeclaBool(String[] cad)
    {
      int i,j=0;
      for (i=0;i<cad.length;i++)
      {
          
          
          Pattern pat = Pattern.compile("[a-zA-Z-0-9,;=_-]+");
          
          Matcher mat = pat.matcher(cad[i]);
          if (mat.matches()) 
          {
            if (!cad[i].equals("boolean"))
            {
                //System.out.println(cad[i]);
               Pattern pat2 = Pattern.compile("[a-zA-Z]+[0-9]*"); 
               Matcher mat2 = pat2.matcher(cad[i]);
               if (mat2.matches())
               {
                  // System.out.println("si");
                   
                   arrBool[movBool]=cad[i];
                   if(arrBool[movBool].equals("false") 
                           || arrBool[movBool].equals("true"))
                    {
                        arrBool[movBool]="";
                        movBool--;
                    }
                   movBool++;
               } else
               {
                
                
                   // System.out.println("si");
                   // System.out.println(cad[i]);
                    boolean igual = false;
                    boolean coma = false;
                    for (j=0;j<cad[i].length()-1;j++)
                    {
                        
                            switch (cad[i].charAt(j))
                            {
                                case '=':
                                {
                                    igual= true ;
                                    break;
                                }
                                
                                case ',':
                                {
                                    coma= true ;
                                    igual= false ;
                                    movBool++;
                                    break ;
                                }
                                
                                case ';':
                                {
                                    j=cad[i].length();
                                    break ;
                                }
                                
                                
                                
                                default :
                                {
                                  Pattern pat3 = Pattern.compile
                                    ("[a-zA-Z]+[0-9]*"); 
                                  Matcher mat3 = pat3.matcher
                                    (Character.toString(cad[i].charAt(j)));
                                  if (mat3.matches())  
                                  {
                                    if (igual==false)
                                    {
                                        arrBool[movBool]+=cad[i].charAt(j);
                                        
                                    }
                                  } 
                                }
                                        
                            
                            
                            }
                            
                    }
                    if(arrBool[movBool].equals("false") 
                            || arrBool[movBool].equals("true"))
                    {
                        arrBool[movBool]="";
                        movBool--;
                    }
                   movBool++;
                   // System.out.println("");
                
               }
               
            }
          }
          
      }  
    }
    
    
    //Metodo para encontrar Declaraciones de char//
    
    public static void DeclaChar(String[] cad)
    {
      int i,j=0;
      boolean empiezaCad=false;
      for (i=0;i<cad.length;i++)
      {
           
          
          Pattern pat = Pattern.compile("[a-zA-Z-0-9,;\"\'=_-]+");
          
          Matcher mat = pat.matcher(cad[i]);
          if (mat.matches()) 
          {
            if (!cad[i].equals("char"))
            {
                
                //System.out.println(cad[i]);
               Pattern pat2 = Pattern.compile("[a-zA-Z]+[0-9_]*"); 
               Matcher mat2 = pat2.matcher(cad[i]);
               if (mat2.matches() && empiezaCad==false)
               {
                  // System.out.println("si");
                   arrChar[movChar]=cad[i];
                   movChar++;
               } else
               {
                   
                   // System.out.println("si");
                   // System.out.println(cad[i]);
                    boolean igual = false;
                    boolean coma = false;
                  
                    
                    for (j=0;j<cad[i].length()-1;j++)
                    {
                        
                        
                            switch (cad[i].charAt(j))
                            {
                                case '=':
                                {
                                    igual= true ;
                                    break;
                                }
                                
                                case ',':
                                {
                                    coma= true ;
                                    igual= false ;
                                    movChar++;
                                    break ;
                                }
                                
                                case ';':
                                {
                                    j=cad[i].length();
                                    break ;
                                }
                                
                                case '\'':
                                {
                                    if(empiezaCad==false)
                                    {
                                        empiezaCad=true;
                                    }else
                                    {
                                        empiezaCad=false;
                                    }
                                    
                                  break;
                                }
                                
                                default :
                                {
                                    System.out.println(cad[i]);
                                  Pattern pat3 = Pattern.compile
                                    ("[a-zA-Z]+[0-9]*"); 
                                  Matcher mat3 = pat3.matcher
                                    (Character.toString(cad[i].charAt(j)));
                                  if (mat3.matches() || cad[i].charAt(j)=='_')  
                                  {
                                       
                                    if (igual==false && empiezaCad==false)
                                    {
                                        arrChar[movChar]+=cad[i].charAt(j);
                                    }
                                  } 
                                }
                                
                               
                            }
                            
                    }
                   movChar++;
                   // System.out.println("");
                
               }
               
            }
          }
          
      }  
    }
    
    
    //Metodo para encontrar Declaraciones de byte//
    public static void DeclaByte(String[] cad)
    {
      int i,j=0;
      for (i=0;i<cad.length;i++)
      {
          
          
          Pattern pat = Pattern.compile("[a-zA-Z-0-9,;=_-]+");
          
          Matcher mat = pat.matcher(cad[i]);
          if (mat.matches()) 
          {
            if (!cad[i].equals("byte"))
            {
                //System.out.println(cad[i]);
               Pattern pat2 = Pattern.compile("[a-zA-Z]+[0-9]*"); 
               Matcher mat2 = pat2.matcher(cad[i]);
               if (mat2.matches())
               {
                  // System.out.println("si");
                   arrByte[movByte]=cad[i];
                   movByte++;
               } else
               {
                
                
                   // System.out.println("si");
                   // System.out.println(cad[i]);
                    boolean igual = false;
                    boolean coma = false;
                    for (j=0;j<cad[i].length()-1;j++)
                    {
                        
                            switch (cad[i].charAt(j))
                            {
                                case '=':
                                {
                                    igual= true ;
                                    break;
                                }
                                
                                case ',':
                                {
                                    coma= true ;
                                    igual= false ;
                                    movByte++;
                                    break ;
                                }
                                
                                case ';':
                                {
                                    j=cad[i].length();
                                    break ;
                                }
                                
                                
                                
                                default :
                                {
                                  Pattern pat3 = Pattern.compile
                                    ("[a-zA-Z]+[0-9]*"); 
                                  Matcher mat3 = pat3.matcher
                                    (Character.toString(cad[i].charAt(j)));
                                  if (mat3.matches())  
                                  {
                                    if (igual==false)
                                    {
                                        arrByte[movByte]+=cad[i].charAt(j);
                                    }
                                  } 
                                }
                                        
                            
                            
                            }
                            
                    }
                   movByte++;
                   // System.out.println("");
                
               }
               
            }
          }
          
      }  
    }
    
    //Metodo para encontrar Declaraciones de short//
    public static void DeclaShort(String[] cad)
    {
      int i,j=0;
      for (i=0;i<cad.length;i++)
      {
          
          
          Pattern pat = Pattern.compile("[a-zA-Z-0-9,;=_-]+");
          
          Matcher mat = pat.matcher(cad[i]);
          if (mat.matches()) 
          {
            if (!cad[i].equals("short"))
            {
                //System.out.println(cad[i]);
               Pattern pat2 = Pattern.compile("[a-zA-Z]+[0-9]*"); 
               Matcher mat2 = pat2.matcher(cad[i]);
               if (mat2.matches())
               {
                  // System.out.println("si");
                   arrShort[movShort]=cad[i];
                   movShort++;
               } else
               {
                
                
                   // System.out.println("si");
                   // System.out.println(cad[i]);
                    boolean igual = false;
                    boolean coma = false;
                    for (j=0;j<cad[i].length()-1;j++)
                    {
                        
                            switch (cad[i].charAt(j))
                            {
                                case '=':
                                {
                                    igual= true ;
                                    break;
                                }
                                
                                case ',':
                                {
                                    coma= true ;
                                    igual= false ;
                                    movShort++;
                                    break ;
                                }
                                
                                case ';':
                                {
                                    j=cad[i].length();
                                    break ;
                                }
                                
                                
                                
                                default :
                                {
                                  Pattern pat3 = Pattern.compile
                                    ("[a-zA-Z]+[0-9]*"); 
                                  Matcher mat3 = pat3.matcher
                                    (Character.toString(cad[i].charAt(j)));
                                  if (mat3.matches())  
                                  {
                                    if (igual==false)
                                    {
                                        arrShort[movShort]+=cad[i].charAt(j);
                                    }
                                  } 
                                }
                                        
                            
                            
                            }
                            
                    }
                   movShort++;
                   // System.out.println("");
                
               }
               
            }
          }
          
      }  
    }
    
   //Metodo para encontrar Declaraciones de Long//
    public static void DeclaLong(String[] cad)
    {
      int i,j=0;
      for (i=0;i<cad.length;i++)
      {
          
          
          Pattern pat = Pattern.compile("[a-zA-Z-0-9,;=_-]+");
          
          Matcher mat = pat.matcher(cad[i]);
          if (mat.matches()) 
          {
            if (!cad[i].equals("long"))
            {
                //System.out.println(cad[i]);
               Pattern pat2 = Pattern.compile("[a-zA-Z]+[0-9]*"); 
               Matcher mat2 = pat2.matcher(cad[i]);
               if (mat2.matches())
               {
                  // System.out.println("si");
                   arrLong[movLong]=cad[i];
                   movLong++;
               } else
               {
                
                
                   // System.out.println("si");
                   // System.out.println(cad[i]);
                    boolean igual = false;
                    boolean coma = false;
                    for (j=0;j<cad[i].length()-1;j++)
                    {
                        
                            switch (cad[i].charAt(j))
                            {
                                case '=':
                                {
                                    igual= true ;
                                    break;
                                }
                                
                                case ',':
                                {
                                    coma= true ;
                                    igual= false ;
                                    movLong++;
                                    break ;
                                }
                                
                                case ';':
                                {
                                    j=cad[i].length();
                                    break ;
                                }
                                
                                
                                
                                default :
                                {
                                  Pattern pat3 = Pattern.compile
                                    ("[a-zA-Z]+[0-9]*"); 
                                  Matcher mat3 = pat3.matcher
                                    (Character.toString(cad[i].charAt(j)));
                                  if (mat3.matches())  
                                  {
                                    if (igual==false)
                                    {
                                        arrLong[movLong]+=cad[i].charAt(j);
                                    }
                                  } 
                                }
                                        
                            
                            
                            }
                            
                    }
                   movLong++;
                   // System.out.println("");
                
               }
               
            }
          }
          
      }  
    }
    
    //Metodo para encontrar Declaraciones de float//
     public static void DeclaFloat(String[] cad)
    {
      int i,j=0;
      for (i=0;i<cad.length;i++)
      {
          
          Pattern pat = Pattern.compile("[a-zA-Z-0-9,.;=_-]+");
          
          Matcher mat = pat.matcher(cad[i]);
          if (mat.matches()) 
          {
            if (!cad[i].equals("float"))
            {
                //System.out.println(cad[i]);
               Pattern pat2 = Pattern.compile("[a-zA-Z]+[0-9]*"); 
               Matcher mat2 = pat2.matcher(cad[i]);
               if (mat2.matches())
               {
                  // System.out.println("si");
                   arrFloat[movFloat]=cad[i];
                   movFloat++;
               } else
               {
                
                
                   // System.out.println("si");
                   // System.out.println(cad[i]);
                    boolean igual = false;
                    boolean coma = false;
                    for (j=0;j<cad[i].length()-1;j++)
                    {
                        
                            switch (cad[i].charAt(j))
                            {
                                case '=':
                                {
                                    igual= true ;
                                    break;
                                }
                                
                                case ',':
                                {
                                    coma= true ;
                                    igual= false ;
                                    movLong++;
                                    break ;
                                }
                                
                                case ';':
                                {
                                    j=cad[i].length();
                                    break ;
                                }
                                
                                
                                
                                default :
                                {
                                  Pattern pat3 = Pattern.compile
                                    ("[a-zA-Z]+[0-9]*"); 
                                  Matcher mat3 = pat3.matcher
                                    (Character.toString(cad[i].charAt(j)));
                                  if (mat3.matches())  
                                  {
                                    if (igual==false)
                                    {
                                        arrFloat[movFloat]+=cad[i].charAt(j);
                                    }
                                  } 
                                }
                                        
                            
                            
                            }
                            
                    }
                   movFloat++;
                   // System.out.println("");
                
               }
               
            }
          }
          
      }  
    }
    
     //Metodo para encontrar Declaraciones de double//
     public static void DeclaDouble(String[] cad)
    {
      int i,j=0;
      for (i=0;i<cad.length;i++)
      {
          
          Pattern pat = Pattern.compile("[a-zA-Z-0-9,.;=_-]+");
          
          Matcher mat = pat.matcher(cad[i]);
          if (mat.matches()) 
          {
            if (!cad[i].equals("double"))
            {
                //System.out.println(cad[i]);
               Pattern pat2 = Pattern.compile("[a-zA-Z]+[0-9]*"); 
               Matcher mat2 = pat2.matcher(cad[i]);
               if (mat2.matches())
               {
                  // System.out.println("si");
                   arrDouble[movDouble]=cad[i];
                   movDouble++;
               } else
               {
                
                   // System.out.println("si");
                   // System.out.println(cad[i]);
                    boolean igual = false;
                    boolean coma = false;
                    for (j=0;j<cad[i].length()-1;j++)
                    {
                        
                            switch (cad[i].charAt(j))
                            {
                                case '=':
                                {
                                    igual= true ;
                                    break;
                                }
                                
                                case ',':
                                {
                                    coma= true ;
                                    igual= false ;
                                    movDouble++;
                                    break ;
                                }
                                
                                case ';':
                                {
                                    j=cad[i].length();
                                    break ;
                                }
                                
                                
                                default :
                                {
                                  Pattern pat3 = Pattern.compile
                                    ("[a-zA-Z]+[0-9]*"); 
                                  Matcher mat3 = pat3.matcher
                                    (Character.toString(cad[i].charAt(j)));
                                  if (mat3.matches())  
                                  {
                                    if (igual==false)
                                    {
                                        arrDouble[movDouble]+=cad[i].charAt(j);
                                    }
                                  } 
                                }
                            }
                    }
                   movDouble++;
                   // System.out.println("");
               }    
            }
          }     
      }  
    }    
}
