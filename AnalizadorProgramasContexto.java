
package analizadorprogramascontexto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AnalizadorProgramasContexto {

   
      static  int histograma [] = new int [4],pos;
    
      static   String tkns_cond [] = {"if","switch"};
      static   String tkns_impr [] = {"System.out.print","System.out.println"};
      static   String tkns_asign [] = {"+=","-=","*=","/=","%="};
      static   String tkns_itera[] = {"for","while","do"};
      
    
    
    public static void main(String[] args) {
        
        try {
         
            boolean posible =false;
            int caracter,mayor=0,contexto=0;
      FileReader fr = new FileReader("arch.txt");
      caracter = fr.read();
      String cad="";
            
            
      
      while (caracter != -1 )  //Almacena el renglon en linea
      {
          
          
          //System.out.print((char)caracter);
          

           if(((char)caracter!=' ') && ((char)caracter!='\t'))
           {
               
                cad+=(char)caracter;
                   
               
                if(analisis_condicion(cad)==true)
                {
                    posible=true;
                  //System.out.println("Si es cond");
                }
                
                if(analisis_iteracion(cad)==true)
                {
                    posible=true;
                  //  System.out.println("Si es itera");
                }
                
                if(analisis_asignacion(cad)==true)
                {
                    posible=true;
                }
                
                if(analisis_impresion(cad)==true)
                {
                    posible=true;
                }
                else
                {
                   // System.out.println("No es");
                    if(posible==false)
                    {
                    cad="";
                    }
                }
                posible=false;
               // System.out.println(cad+"\n");
           }
           
            caracter = fr.read();
           
          // System.out.println(caracter);
         
      }
            
            System.out.println("Condicional\tImpresion\tAsignacion\tIteracion");
            
               for(int i=0;i<histograma.length;i++)
               {
                   System.out.print(histograma[i]+"\t\t ");
                   if(histograma[i]>mayor)
                   {
                       mayor=histograma[i];
                       contexto=i;
                   }
               }
               System.out.println("");
               System.out.print("\nEl programa esta en el contexto: ");
               
               switch(contexto)
               {
                   case 0:
                   {
                       System.out.println("Condicional");
                       break;
                   }
                   case 1:
                   {
                       System.out.println("Impresion");
                       break;
                   }
                   case 2:
                   {
                       System.out.println("Asignacion");
                       break;
                   }
                   case 3:
                   {
                       System.out.println("Iteracion");
                       break;
                   }
                   
               }
            
      fr.close();
            
       
    }
        catch (Exception e) 
        {
          System.out.println("Excepcion leyendo fichero" + e); 
        }
    }
      ////Estos analisis es para asesorarse de que algun tken sea igual a
    //la cadena obtenida//
    static boolean analisis_condicion(String cad)
    {
     int i;
        
     
    try
    {
     for(i=0;i<tkns_cond.length;i++)
     {
         if(analisis_tknVScad(tkns_cond[i], cad)==true)
         {
            if(cad.length()<=tkns_cond[i].length())
            {
                return true;
            }else
            {
                // System.out.println(pos);
               //  System.out.println(cad);
                if(cad.length()>=pos)
                    
                if(cad.charAt(pos)=='(')
                {
                    histograma[0]+=1;
                    return false;
                }
            }
         }
     }
    }
    catch (StringIndexOutOfBoundsException e)
    {
        System.out.println("Algun error por el conteo");
    }
     return false;
    }
    
    
    static boolean analisis_impresion(String cad)
    {
        int i;
        
     
    try
    {
     for(i=0;i<tkns_impr.length;i++)
     {
         if(analisis_tknVScad(tkns_impr[i], cad)==true)
         {
            if(cad.length()<=tkns_impr[i].length())
            {
                return true;
            }else
            {
                // System.out.println(pos);
               //  System.out.println(cad);
                if(cad.length()>=pos)
                if(cad.charAt(pos)=='(')
                {
                    histograma[1]+=1;
                    return false;
                }
                
            }
         }
     }
    }
    catch (StringIndexOutOfBoundsException e)
    {
        System.out.println("Algun error por el conteo");
    }
     return false; 
        
    }
    
    static boolean analisis_asignacion(String cad)
    {
        int i;
        
     
    try
    {
     for(i=0;i<tkns_asign.length;i++)
     {
         if(analisis_tknVScad(tkns_asign[i], cad)==true)
         {
            if(cad.length()<=tkns_asign[i].length())
            {
                return true;
            }else
            {
                // System.out.println(pos);
               //  System.out.println(cad);
                if(cad.length()>=pos)
                    
                
                    histograma[2]+=1;
                 
                
            }
         }
     }
    }
    catch (StringIndexOutOfBoundsException e)
    {
        System.out.println("Algun error por el conteo");
    }
     return false;
       
    }
    
    static boolean analisis_iteracion(String cad)
    {
        
     int i;
     
    try
    {
     for(i=0;i<tkns_itera.length;i++)
     {
         if(analisis_tknVScad(tkns_itera[i], cad)==true)
         {
            if(cad.length()<=tkns_itera[i].length())
            {
                return true;
            }else
            {
                // System.out.println(pos);
               //  System.out.println(cad);
                if(cad.length()>=pos)
                    
                if(cad.charAt(pos)=='(')
                {
                    histograma[3]+=1;
                    return false;
                }
            }
         }
     }
    }
    catch (StringIndexOutOfBoundsException e)
    {
        System.out.println("Algun error por el conteo");
    }
     return false;
        
        
        
    }
    
   ///// Para analizar el token y la cadena dada /////
    static boolean analisis_tknVScad(String tkn,String cad)
    {
     int i;
     boolean igual=false;
     String mayor,menor;
     
     if(tkn.length()>cad.length())
     {
        mayor=tkn;
        menor=cad;
     }else
     {
        mayor=cad;
        menor=tkn;
     }
     
     try {
     for(i=0;i<mayor.length();i++)
     {
         for(pos=0;pos<menor.length();pos++)
         {
           //  System.out.println(cad);
            // System.out.println(tkn);
             if(mayor.charAt(pos)==menor.charAt(pos))
             {
                 igual=true;
             }else
             {
                 return false;
             }
            
         }
        // System.out.println("");
         if(igual==true)
         {
             return true;
         }
     }
     }
     catch(StringIndexOutOfBoundsException e)
     {
         System.out.println("Algun error en la comparacion");
     }
      return false;
    }  
}
