
package proyectofinalautomatas2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;


public class ProyectoFinalAutomatas2 {

     static String tkn_accion_imprimir [] = {"muestrame","muestra","muestre","imprime","imprimas","imprima","ver","imprimir"};
     static String tkn_accion_operacion [] = {"operacion","operaciones","resultado"};
     static String tkn_accion_comparacion [] = {"compara","compares","analices","compare","comparas","comparar"};
     static String tkn_accion_tipoComparacion [] = {"mayor","menor","igual","iguales"};
     static String tkn_accion_llenar_matrices [] = {"llenar","llenado","agregar","refill","rellenar","rellenado","rellene","llene"};
     static String tkn_accion_sumas_mat [] = {"sumes","sumar","suma","sume"};
     static String tkn_accion_restas_mat [] = {"restes","restar","resta","reste"};
     
    public static void main(String[] args) {
        
         try {
      FileReader fr = new FileReader("natural.txt");
      BufferedReader br = new BufferedReader(fr);
      
      String linea;
      int i;
      
      while ((linea = br.readLine()) != null )  //Almacena el renglon en linea
      {
          
          String [] palabras = linea.split(" ");
          
          for(i=0;i<palabras.length;i++)
          {   
              if(comparador(palabras[i],tkn_accion_imprimir)==true)
                {
                    i++;
                    accion_imprimir(palabras,i);
                }   
              if(comparador(palabras[i], tkn_accion_operacion)==true)
                {
                     i++;
                     accion_operacion(palabras,i);
                     
                }
               if(comparador(palabras[i], tkn_accion_comparacion)==true)
                {
                     i++;
                     accion_comparacion(palabras,i);    
                }
               if(comparador(palabras[i], tkn_accion_llenar_matrices)==true)
                {
                     i++;
                     accion_rellenado_matrices(palabras,i);  
                }
               if(comparador(palabras[i], tkn_accion_sumas_mat)==true)
                {
                     i++;
                     accion_operacion_matrices(palabras,i,"suma");
                }
               if(comparador(palabras[i], tkn_accion_restas_mat)==true)
                {
                     i++;
                     accion_operacion_matrices(palabras,i,"resta");
                } 
          }
           System.out.println("No entiendo la accion que quieres hacer, revisa las posibilidades");
      }
      fr.close();
    }
    catch (Exception e) {
      System.out.println("Excepcion leyendo fichero" + e);
    }   
    }
    
    
    static boolean comparador(String palabra,String tkn[])
    {
        int i;
        
        for(i=0;i<tkn.length;i++)
        {
            if(tkn[i].equals(palabra))
            {
                return true;
            }
        }
        return false;
    }
    
    static void accion_imprimir(String oracion[],int pos)
    {
    int veces=1;
    boolean comillas=false,dos=false;
    String to_print=" ";
    
    for(;pos<oracion.length;pos++)
    {
        try
        {
       try
       {
           if(comillas==false)
           {
           veces=Integer.parseInt(oracion[pos]);//Si es un numero
          
         //  System.out.println("si es numero");
           pos++;
           }
       }
       catch (NumberFormatException e)  //Si no es un numero
       {
           //System.out.println("No es numero");
       }
       try
       {
        if(oracion[pos].charAt(0)=='"')
        {
          // System.out.println("Lo tiene");
           
           pos++;
           if(comillas==false)
           {
               comillas=true;
           }else
           {
               pos--;
               dos=true;
               comillas=false;
           }
        }
       }
       catch(StringIndexOutOfBoundsException e)
       {
        System.out.println("No entiendo que quieres imprimir");
        System.exit(0);
       }
        if(comillas==true)
        {
           
            to_print+=oracion[pos];
             to_print+=" ";
        }
        
        }catch (ArrayIndexOutOfBoundsException a)
        {
            System.out.println("No entiendo que quieres imprimir");
        System.exit(0);
        }
    }
       // System.out.println(comillas);
    
        if(dos==true)
        {
        System.out.println("Creando programa en C# que imprima: \""
        +to_print+"\" "+veces+" veces");
        pasar_cs("imprimir",to_print,veces,true,"no","no");
        }else
        {
            System.out.println("No entiendo que quieres imprimir");
        System.exit(0);
        }
       
    }
    
    static void pasar_cs(String accion,String cad,int veces,boolean entero, String tipo,String cad2)
    {
    
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("C:/Users/as-la/source/repos/proyfinalautomatas/proyfinalautomatas/Program.cs");
            pw = new PrintWriter(fichero);

            pw.println("using System;");    
                pw.println("namespace proyfinal"); 
                pw.println("{"); 
                pw.println("\tpublic class proy"); 
                pw.println("\t{"); 
                if(accion.equals("operacion_matrices"))
                {
                pw.println("\t\tprivate int[,] MatrizA;\n" +
                           "\t\tprivate int[,] MatrizB;\n" +
                           "\t\tprivate int[,] MatrizC;\n"); 
                
                }
                pw.println("\t\tstatic void Main(string[] args)"); 
                pw.println("\t\t{"); 
            
            switch (accion)
            {
                case "imprimir":
                {
                    if(veces==1)
                    {
                       
                pw.println("\t\t\tConsole.WriteLine(\""+cad+"\");"); 
               
                    }else
                    {
                 
                pw.println("\t\t\tint i;");
                pw.println("\t\t\tfor(i=0;i<"+veces+";i++)");
                pw.println("\t\t\t{");
                pw.println("\t\t\t\t\tConsole.WriteLine(\""+cad+"\");"); 
                pw.println("\t\t\t}");
                    
                    }
                break;
                }
                
                case "operacion":
                {
                   if(entero==true)
                   {
                   pw.println("\t\t\tint i="+cad+";");
                   pw.println("\t\t\t\t\tConsole.WriteLine(i);"); 
                   }else
                   {
                   pw.println("\t\t\tdouble i="+cad+";");
                   pw.println("\t\t\t\t\tConsole.WriteLine(i);");
                   }
                
                
                break;
                }
                
                case "comparacion":
                {
                   
                    switch(tipo)
                    {
                    
                    case "mayor":
                    {
                   pw.println("\t\t\tint a="+cad+";");
                   pw.println("\t\t\tint b="+cad2+";");
                   pw.println("\t\t\tif (a>b)");
                   pw.println("\t\t\t\t\tConsole.WriteLine(a+\" Es el numero mayor\");"); 
                   pw.println("\t\t\telse");
                   pw.println("\t\t\t\t\tConsole.WriteLine(b+\" Es el numero mayor\");");
                        break;
                    }
                    
                    case "menor":
                    {
                         pw.println("\t\t\tint a="+cad+";");
                   pw.println("\t\t\tint b="+cad2+";");
                   pw.println("\t\t\tif (a>b)");
                   pw.println("\t\t\t\t\tConsole.WriteLine(b+\" Es el numero menor\");"); 
                   pw.println("\t\t\telse");
                   pw.println("\t\t\t\t\tConsole.WriteLine(a+\" Es el numero menor\");");
                    
                        break;
                    }
                    
                    case "igual":
                    case "iguales":
                    {
                   pw.println("\t\t\tint a="+cad+";");
                   pw.println("\t\t\tint b="+cad2+";");
                   pw.println("\t\t\tif (a==b)");
                   pw.println("\t\t\t\t\tConsole.WriteLine(\" Los dos numeros son iguales \");"); 
                   pw.println("\t\t\telse");
                   pw.println("\t\t\t\t\tConsole.WriteLine(\" Son numeros diferentes \");");
                    
                        break;
                    }
                    
                    }
                    
                    
                break;
                }
                
                case "rellenar":
                    {   
                    pw.println("\t\t\tint i, j,num;");
                    pw.println("\t\t\tString Line;");
                    pw.println("\t\t\tint[,] arr = new int["+cad+","+cad2+"];");
                    pw.println("\t\t\tfor (i = 0; i < arr.GetLength(0); i++)");
                    pw.println("\t\t\t{");
                    pw.println("\t\t\t\tfor(j=0;j<arr.GetLength(1);j++)");
                    pw.println("\t\t\t\t{");
                    pw.println("\t\t\t\t\tConsole.Write(\"Introduzca el numero de la posicion: [\"+i+\",\"+j+ \"] (Solo numeros)\\n\");");
                    pw.println("\t\t\t\t\tLine = Console.ReadLine();");
                    pw.println("\t\t\t\t\tarr[i, j] = int.Parse(Line);");
                    pw.println("\t\t\t\t}");
                    pw.println("\t\t\t}");
                    pw.println("\t\t\tConsole.WriteLine(\"Imprimiendo matriz: \\n\");");
                     pw.println("\t\t\t");
                    pw.println("\t\t\tfor (i = 0; i < arr.GetLength(0); i++)");
                    pw.println("\t\t\t{");
                    pw.println("\t\t\t\tfor (j = 0; j < arr.GetLength(1); j++)");
                    pw.println("\t\t\t\t{");
                    pw.println("\t\t\t\t\tConsole.Write(\"\\t\"+arr[i,j]);");
                    pw.println("\t\t\t\t}");
                    pw.println("\t\t\t\t\tConsole.Write(\"\\n\");");
                    pw.println("\t\t\t}");
                    break;
                    }
                    
                case "operacion_matrices":
                {
                    String tip="";
                    if(tipo.equals("suma"))
                    {
                        tip="+";
                    }else
                    {
                        tip="-";
                    }
                pw.println("\t\t\tproy pv = new proy();\n" +
                           "\t\t\tpv.Cargar();\n" +
                           "\t\t\tpv.visualizar();\n" +
                           "\t\t}\n");
                
                pw.println("\t\tpublic void Cargar()\n" +
                           "\t\t{\n" +
                           "\t\t\tint ren = "+cad+" , col= "+cad2+";\n" +
                           "\t\t\tMatrizA = new int[ren, col];\n" +
                           "\t\t\tMatrizB = new int[ren, col];\n" +
                           "\t\t\tMatrizC = new int[ren, col];\n" +
                           "\t\t\t\n" +
                           "\t\t\tConsole.WriteLine(\"Ingresando datos al matriz A\");\n" +
                           "\t\t\tfor (int i = 0; i < MatrizB.GetLength(0); i++)\n" +
                           "\t\t\t{\n" +
                           "\t\t\t\tfor (int j = 0; j < MatrizB.GetLength(1); j++)\n" +
                           "\t\t\t\t{\n" +
                           "\t\t\t\t\tConsole.Write(\"Ingrese posicion [\" + i + \",\" + j + \"]: \");\n" +
                           "\t\t\t\t\tstring linea;\n" +
                           "\t\t\t\t\tlinea = Console.ReadLine();\n" +
                           "\t\t\t\t\tMatrizA[i, j] = int.Parse(linea);\n" +
                           "\t\t\t\t}\n" +
                           "\t\t\t}\n" +
                           "\t\t\t\n" +
                           "\t\t\tConsole.WriteLine(\"Ingresando datos al matriz B\");\n" +
                           "\t\t\tfor (int i = 0; i < MatrizB.GetLength(0); i++)\n" +
                           "\t\t\t{\n" +
                           "\t\t\t\tfor (int j = 0; j < MatrizB.GetLength(1); j++)\n" +
                           "\t\t\t\t{\n" +
                           "\t\t\t\t\tConsole.Write(\"Ingrese posicion [\" + i + \",\" + j + \"]: \");\n" +
                           "\t\t\t\t\tstring linea;\n" +
                           "\t\t\t\t\tlinea = Console.ReadLine();\n" +
                           "\t\t\t\t\tMatrizB[i, j] = int.Parse(linea);\n" +
                           "\t\t\t\t}\n" +
                           "\t\t\t}\n" +
                           "\t\t\t\n" +
                           "\t\t\t//Operacion entre la matrizA y la MatrizB\n" +
                           "\t\t\tfor (int i = 0; i < MatrizA.GetLength(0); i++)\n" +
                           "\t\t\t{\n" +
                           "\t\t\t\tfor (int j = 0; j < MatrizA.GetLength(1); j++)\n" +
                           "\t\t\t\t{\n" +
                           "\t\t\t\t\tMatrizC[i, j] = MatrizA[i, j] "+tip+" MatrizB[i, j];\n" +
                           "\t\t\t\t}\n" +
                           "\t\t\t}\n" +
                           "\t\t}\n");
                
                pw.println("\t\t// visualizamos la suma de las matrices\n" +
                           "\t\tpublic void visualizar()\n" +
                           "\t\t{\n" +
                           "\t\t\tint cont = 0;\n" +
                           "\t\t\tConsole.WriteLine(\"La "+tipo+" de la MatrizA y MatrizB es :\");\n" +
                           "\t\t\tfor (int i = 0; i < MatrizA.GetLength(0); i++)\n" +
                           "\t\t\t{\n" +
                           "\t\t\t\tConsole.Write(\"\\n\\t\");\n" +
                           "\t\t\t\tfor (int j = 0; j < MatrizA.GetLength(1); j++)\n" +
                           "\t\t\t\t{\n" +
                           "\t\t\t\t\n" +
                           "\t\t\t\t\tConsole.Write(MatrizA[i, j] + \"  \");\n" +
                           "\t\t\t\t}\n" +
                           "\t\t\t}\n" +
                           "\t\t\t\n" +
                           "\t\t\tConsole.Write(\"\\n\\n\\t"+tip+"\\n\\n\");\n" +
                           "\t\t\t\n" +
                           "\t\t\tfor (int i = 0; i < MatrizB.GetLength(0); i++)\n" +
                           "\t\t\t{\n" +
                           "\t\t\t\tConsole.Write(\"\\n\\t\");\n" +
                           "\t\t\t\tfor (int j = 0; j < MatrizB.GetLength(1); j++)\n" +
                           "\t\t\t\t{\n" +
                           "\t\t\t\t\n" +
                           "\t\t\t\t\tConsole.Write(MatrizB[i, j] + \"  \");\n" +
                           "\t\t\t\t}\n" +
                           "\t\t\t}\n" +
                           "\t\t\t\t\n" +
                           "\t\t\tConsole.Write(\"\\n\\n\\t=\\n\\n\");\n" +
                           "\t\t\t\n" +
                           "\t\t\t\n" +
                           "\t\t\tfor (int i = 0; i < MatrizC.GetLength(0); i++)\n" +
                           "\t\t\t{\n" +
                           "\t\t\t\tConsole.Write(\"\\n\\t\");\n" +
                           "\t\t\t\tfor (int j = 0; j < MatrizC.GetLength(1); j++)\n" +
                           "\t\t\t\t{\n" +
                           "\t\t\t\t\n" +
                           "\t\t\t\t\tConsole.Write(MatrizC[i, j] + \"  \");\n" +
                           "\t\t\t\t}\n" +
                           "\t\t\t}\n" +
                           "\t\t\t\n" +
                           "\t\t\t\n" +
                           "\t\t\tConsole.ReadKey();\n" +
                           "\t\n");
                }
            }
             pw.println("\t\t}"); 
                pw.println("\t}"); 
                pw.println("}");
                
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
           System.exit(0);
        }
    }
    
    static void accion_operacion(String oracion[],int pos)
    {
    int j;
    boolean comillas=false,entero,dos=false;
    String to_print=" ";
    
    for(;pos<oracion.length;pos++)
    {
        try
        {
       
       try
       {
        if(oracion[pos].charAt(0)=='"')
        {
          // System.out.println("Lo tiene");
           
           pos++;
           if(comillas==false)
           {
               comillas=true;
           }else
           {
               pos--;
               dos=true;
               comillas=false;
           }
        }
       }
       catch(StringIndexOutOfBoundsException e)
       {
        System.out.println("No entiendo tu operacion");
        System.exit(0);
       }
        if(comillas==true)
        {
           
            to_print+=oracion[pos];
             to_print+=" ";
        }
        
        }catch (ArrayIndexOutOfBoundsException a)
        {
            System.out.println("No entiendo tu operacion");
        System.exit(0);
        }
    }
    if(dos==true)
    {
        entero=revisar_opera(to_print);
    
        
        System.out.println("Creando programa en C# que imprima el resultado de: \""
        +to_print+"\"");
        //System.out.println(entero);
        pasar_cs("operacion",to_print,1,entero,"no","no");
    }else
    {
    System.out.println("No entiendo tu operacion");
        System.exit(0);
    }  
    }
    
    static boolean revisar_opera(String operacion)
    {
        int i,a;
        boolean entero=true;
        for(i=1;i<operacion.length()-1;i++)
        {
            try
            {
           a=Integer.parseInt(Character.toString(operacion.charAt(i)));
           // System.out.println(operacion.charAt(i));     
           //System.out.println("numero");
            }
            catch (NumberFormatException e)
            {
                switch (operacion.charAt(i))
                {
                    case '/':
                    case '.':
                        entero=false;
                    case '+':
                    case '-':
                    case '*':
                    case '(':
                    case ')':
                    case ' ':
                    {
                      //  System.out.println(operacion.charAt(i)); 
                       // System.out.println("operacion");
                        break;
                    }   
                    default:
                    {
                        System.out.println("Error, no escribiste numeros u "
                                + "operadores aritmeticos entre las comillas: \""+operacion.charAt(i)+"\"");
                        System.exit(0);
                        break;
                    }
                }
                
            }
        }
        
    return entero;
    }
    
    static void accion_comparacion(String oracion[],int pos)
    {
    int veces=1,cont=0;
    boolean comillas1=false,comillas2=false,pase_tipo=false
            ,primero=false,dos=false;
    String to_compare1="",to_compare2="",tipo="";
    
    for(;pos<oracion.length;pos++)
    {
        try
        {
     
        
        if(pase_tipo==false)
        {
            pase_tipo=true;
            tipo=analisa_tipo(oracion,pos);
        }
        
        try
       {
           
           if(comillas1==true)
           {
               to_compare1+=oracion[pos];
               veces=Integer.parseInt(oracion[pos]);
               pos++;
           }
            if(comillas2==true)
           {
               to_compare2+=oracion[pos];
               veces=Integer.parseInt(oracion[pos]);
               pos++;
           }
       }
       catch (NumberFormatException e)  //Si no es un numero
       {
        //   System.out.println(oracion[pos]);
        //  System.out.println(pos);
           System.out.println("Debes comparar numeros");
           System.exit(0);
       }
       try
       {
        if(oracion[pos].charAt(0)=='"')
        {
            cont++;
             
             switch (cont)
             {
                 case 1:
                 {
                     comillas1=true;
                     break;
                 }
                 case 2:
                 {
                     comillas1=false;
                     break;
                 }
                 
                 case 3:
                 {
                     comillas2=true;
                     break;
                 }
                 
                 case 4:
                 {
                     comillas2=false;
                     dos=true;
                     break;
                 }
             }
        }
       }
       catch(StringIndexOutOfBoundsException e)
       {
        System.out.println("No entiendo que quieres comparar");
        System.exit(0);
       }
        
        
        }catch(ArrayIndexOutOfBoundsException a)
        {
          System.out.println("No entiendo que quieres comparar");
        System.exit(0);  
        }
    }
    
    if(dos==true)
    {
        System.out.println("Creando programa en C# que compare: \""
        +to_compare1+"\" y \""+to_compare2+"\"");
        pasar_cs("comparacion",to_compare1,1,true,tipo,to_compare2);
    }else
    {
        System.out.println("No entiendo que quieres comparar");
        System.exit(0);
    }  
    }
    static String analisa_tipo(String oracion[],int pos)
    {
        int j;
        
        for(;pos<oracion.length;pos++)
        {
            for(j=0;j<tkn_accion_tipoComparacion.length;j++)
            {
                if(oracion[pos].equals(tkn_accion_tipoComparacion[j]))
                {
                    return oracion[pos];
                }
            }
        }
        
    System.out.println("No agregaste que tipo de comparacion deseas");
        System.exit(0);
    return "";
    }
    
    static void accion_rellenado_matrices(String oracion[],int pos)
    {
    int veces=1,cont=0;
    boolean comillas1=false,comillas2=false,pase_tipo=false
            ,primero=false,dos=false;
    String to_compare1="",to_compare2="",tipo="";
    
    for(;pos<oracion.length;pos++)
    {
      try
      {
        try
       {
           if(comillas1==true)
           {
               to_compare1+=oracion[pos];
               veces=Integer.parseInt(oracion[pos]);
               pos++;
           }
            if(comillas2==true)
           {
               to_compare2+=oracion[pos];
               veces=Integer.parseInt(oracion[pos]);
               pos++;
           }
       }
       catch (NumberFormatException e)  //Si no es un numero
       {
        //   System.out.println(oracion[pos]);
        //  System.out.println(pos);
           System.out.println("Los tamaños de matrices deben ser numeros");
           System.exit(0);
       }
       try
       {
        if(oracion[pos].charAt(0)=='"')
        {
            cont++;
             
             switch (cont)
             {
                 case 1:
                 {
                     comillas1=true;
                     break;
                 }
                 case 2:
                 {
                     comillas1=false;
                     break;
                 }
                 
                 case 3:
                 {
                     comillas2=true;
                     break;
                 }
                 
                 case 4:
                 {
                     comillas2=false;
                     dos=true;
                     break;
                 }
             }
        }
       }
       catch(StringIndexOutOfBoundsException e)
       {
        System.out.println("No entiendo que quieres llenar");
        System.exit(0);
       }
        
      }catch (ArrayIndexOutOfBoundsException a)
      {
          System.out.println("No entiendo que quieres llenar");
        System.exit(0);
      }
    }
    if(dos==true)
    {
        if(to_compare1.contains("-")||to_compare2.contains("-")||to_compare1.equals("0")||to_compare2.equals("0"))
        {
          System.out.println("No puedes poner valores negativos o cero");
        System.exit(0);
        }else
        {
        System.out.println("Creando programa en C# que rellene matrices de : \""
        +to_compare1+"\" por \""+to_compare2+"\"");
        pasar_cs("rellenar",to_compare1,1,true,tipo,to_compare2);
        }
    }else
    {
        System.out.println("No entiendo que quieres llenar");
        System.exit(0);
    } 
    }
    
    static void accion_operacion_matrices(String oracion[],int pos,String opera)
    {
    int veces=1,cont=0;
    boolean comillas1=false,comillas2=false,pase_tipo=false
            ,primero=false,dos=false;
    String to_compare1="",to_compare2="",tipo=opera;
    
    for(;pos<oracion.length;pos++)
    {
      try
      {
        try
       {
           
           if(comillas1==true)
           {
               to_compare1+=oracion[pos];
               veces=Integer.parseInt(oracion[pos]);
               pos++;
           }
            if(comillas2==true)
           {
               to_compare2+=oracion[pos];
               veces=Integer.parseInt(oracion[pos]);
               pos++;
           }
       }
       catch (NumberFormatException e)  //Si no es un numero
       {
        //   System.out.println(oracion[pos]);
        //  System.out.println(pos);
           System.out.println("Los tamaños de matrices deben ser numeros");
           System.exit(0);
       }
       try
       {
        if(oracion[pos].charAt(0)=='"')
        {
           
            cont++;
             
             switch (cont)
             {
                 case 1:
                 {
                     comillas1=true;
                     break;
                 }
                 case 2:
                 {
                     comillas1=false;
                     break;
                 }
                 
                 case 3:
                 {
                     comillas2=true;
                     break;
                 }
                 
                 case 4:
                 {
                     comillas2=false;
                     dos=true;
                     break;
                 }
             }
        }
       }
       catch(StringIndexOutOfBoundsException e)
       {
        System.out.println("No entiendo la "+tipo+" de matrices");
        System.exit(0);
       }
        
      }catch (ArrayIndexOutOfBoundsException a)
      {
          System.out.println("No entiendo la "+tipo+" de matrices");
        System.exit(0);
      }
        
    }
    
    if(dos==true)
    {
        if(to_compare1.contains("-")||to_compare2.contains("-")||to_compare1.equals("0")||to_compare2.equals("0"))
        {
          System.out.println("No puedes poner valores negativos o cero");
        System.exit(0);
        }else
        {
        System.out.println("Creando programa en C# para la "+tipo+" de matrices de : \""
        +to_compare1+"\" por \""+to_compare2+"\"");
        pasar_cs("operacion_matrices",to_compare1,1,true,tipo,to_compare2);
        }
    }else
    {
        System.out.println("No entiendo la "+tipo+" de matrices");
        System.exit(0);
    }
       
    }
      
}
   

