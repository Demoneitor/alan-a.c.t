
package contextos;

import com.sun.webkit.ContextMenu;
import java.awt.JobAttributes;
import java.util.Scanner;
import java.io.*;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Random;


public class Contextos {

    static int cont=0;
    static String diccionarios[] = {"personas","dias","nombre_propio","tiempo",
        "accion"};
    static int histo[] = new int [diccionarios.length];
    public static void main(String[] args) {
        
     
        try {
      FileReader fr = new FileReader("analizar.txt");
      BufferedReader br = new BufferedReader(fr);
      
      String linea;
      
      while ((linea = br.readLine()) != null )//Almacena el renglon en linea
      {
             //Separa la linea en un arreglo de palabras
          String [] palabras = linea.split(" ");
          
          for (int i = 0; i < palabras.length; i++) {
              System.out.println(palabras[i]);
              
              
              Pattern pat = Pattern.compile("[A-Z][a-z]+");
                           Matcher mat = pat.matcher(palabras[i]);
                            if (mat.matches()) 
                            {
                                //System.out.println("Mayus");
                                histo[0]++;
                                histo[2]++;
                            }
                            
                contexto(palabras[i]);
              
          }
          
                
      }
      fr.close();
      
            System.out.println("\n Histograma de contextos:\n ");
            
            int mayor=0,pos=0,i;
            boolean contexto=false;
      
            for (i = 0; i < histo.length; i++) 
        {
            System.out.println(diccionarios[i]+" = "+histo[i]);
            if(histo[i]>mayor)
            {
                mayor=histo[i];
                pos=i;
                contexto=true;
            }
            
        }
            if(contexto==true)
            {
                for(i=0;i<histo.length;i++)
                {
                    if(histo[i]==mayor)
                    {
                        System.out.println("\n Esta en el contexto: "
                                + ""+diccionarios[i]+"\n");
                    }
                }
            }
            else
                System.out.println("\n\nNo esta en alguno de los contextos del "
                        + "diccionario de diccionarios");
    }
        catch (Exception e) {
          System.out.println("Excepcion leyendo fichero" + e);

        }   
    }
    
    
    
    
    
    
        public static void contexto(String palabra)
        {
            try {
                
      int j;
                
                for(j=0;j<diccionarios.length;j++)
                {
                    FileReader fr = new FileReader(diccionarios[j]+".txt");
                    BufferedReader br = new BufferedReader(fr);

                    String linea;

                    while ((linea = br.readLine()) != null )  
                //Almacena el renglon en linea
                    {
                           //Separa la linea en un arreglo de palabras
                        String [] palabras = linea.split(" ");

                        for (int i = 0; i < palabras.length; i++) 
                        {

                           if(palabras[i].equals(palabra))
                           {
                              histo[j]++;
                             //  System.out.println("Mas en "+j);
                           }
                           
                        }

                        }
                         fr.close();
                    }
                    
                   
                }
    
    catch (Exception e) {
      System.out.println("Excepcion leyendo fichero" + e);
       
    }
        }
        
}
